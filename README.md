# simple-vue-deploymant

Vue - DaySpan Application
Easy to install, build and deploy

## Project setup
```
git clone git@gitlab.com:ronibajau/simple-vue-deploymant.git
cd simple-vue-deploymant
npm install
```

### Testing running for development
```
npm run serve
```

### Build for production
```
npm run build
```

### Deploy to target server
```
copy /dist/* /var/www/html/
```

![Vue - Dayspan Apps](/src/assets/apps.png)